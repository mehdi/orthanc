#!/usr/bin/env node
'use strict'

const { writeConfig } = require('./config.js')

writeConfig().then(() => {
  console.log('Orthanc config written.')
})
