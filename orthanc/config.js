'use strict'

const fs = require('fs').promises
const { exec } = require('child_process')
const { promisify } = require('util')

const execAsync = promisify(exec)

const readJson = async (filePath) => {
  const file = await fs.readFile(filePath, { encoding: 'utf8' })
  return JSON.parse(file)
}

const getConfig = async () => {
  const currentConfig = await readJson('/app/data/orthanc/config.json')
  return {
    DicomAet: currentConfig.DicomAet,
    port: process.env.DICOM_PORT || 4242,
    DicomModalities: currentConfig.DicomModalities
  }
}

const changeConfig = async ({ DicomAet, DicomModalities }) => {
  const currentConfig = await readJson('/app/data/orthanc/config.json')
  const newConfig = { ...currentConfig, DicomAet, DicomModalities }
  await fs.writeFile('/app/data/orthanc/config.json', JSON.stringify(newConfig, null, 2))
  await execAsync('supervisorctl stop orthanc')
  await writeConfig()
  await execAsync('supervisorctl start orthanc')
}

const writeConfig = async () => {
  const currentConfig = await readJson('/app/data/orthanc/config.json')
  const hardcodedConfig = await readJson('/app/code/orthanc/hardcoded.json')
  // TODO: postgreSQL
  const totalConfig = { ...currentConfig, ...hardcodedConfig }
  await fs.writeFile('/run/orthanc.json', JSON.stringify(totalConfig, null, 2))
}

module.exports = { getConfig, changeConfig, writeConfig }
