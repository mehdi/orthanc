FROM ohif/viewer:v4.8.1.13663 AS OHIF
FROM jodogne/orthanc:1.8.2 AS ORTHANC
FROM jodogne/orthanc-plugins:1.8.2 AS ORTHANC_PLUGINS


FROM node:12 AS HELPER_BUILDER
RUN mkdir -p /app/code
WORKDIR /app/code
COPY package.json package-lock.json rollup.config.js /app/code/
RUN npm ci
COPY clientSrc /app/code/clientSrc
COPY .gitignore .eslintrc.js /app/code/
RUN npm run build
RUN rm .gitignore .eslintrc.js


FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4
MAINTAINER Mehdi Kouhen <arantes555@gmail.com>

ARG NGINX_VERSION="1.19.6"

## apt stuff
RUN apt-get update -y
RUN apt-get remove -y nginx-full && apt-get autoremove -y
RUN apt-get install -y locales libpcre3 libpcre3-dev

## Locales
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen

## Nginx
RUN mkdir -p /tmp/nginx
WORKDIR /tmp/nginx
RUN wget "https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -O - \
    | tar -xz --strip-components=1 -C /tmp/nginx
RUN ./configure \
    --with-http_auth_request_module \
    --modules-path=/usr/local/nginx/modules \
    --conf-path=/run/nginx.conf \
    --pid-path=/run/nginx.pid \
    --error-log-path=/run/nginx.error.log \
    --build=cloudron-orthanc
RUN make install
RUN rm -rf /tmp/nginx

## Orthanc
WORKDIR /app/code
COPY orthanc/ orthanc/

COPY --from=ORTHANC /usr/local/sbin/Orthanc orthanc/bin/Orthanc
COPY --from=ORTHANC /usr/local/bin/OrthancRecoverCompressedFile orthanc/bin/OrthancRecoverCompressedFile
COPY --from=ORTHANC /usr/local/share/orthanc/plugins/libConnectivityChecks.so orthanc/plugins/libConnectivityChecks.so
COPY --from=ORTHANC_PLUGINS /usr/local/share/orthanc/plugins/libOrthancDicomWeb.so orthanc/plugins/libOrthancDicomWeb.so
# COPY --from=ORTHANC_PLUGINS /usr/local/share/orthanc/plugins/libOrthancPostgreSQLIndex.so orthanc/plugins/libOrthancPostgreSQLIndex.so
# COPY --from=ORTHANC_PLUGINS /usr/local/share/orthanc/plugins/libOrthancPostgreSQLStorage.so orthanc/plugins/libOrthancPostgreSQLStorage.so
### WebViewer plugin : https://book.orthanc-server.com/plugins/osimis-webviewer.html#how-to-get-it
RUN wget "http://orthanc.osimis.io/lsb/plugin-osimis-webviewer/releases/1.3.1/libOsimisWebViewer.so" -O orthanc/plugins/libOsimisWebViewer.so

## Installing file-manager interface && packaging scripts
WORKDIR /app/code
COPY package.json package-lock.json /app/code/
RUN npm ci --production
COPY server /app/code/server
COPY --from=HELPER_BUILDER /app/code/client /app/code/client

## Installing OHIF
COPY --from=OHIF //usr/share/nginx/html /app/code/OHIF
COPY ohif/* /app/code/OHIF/

## Supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf

## config
COPY nginx /app/code/nginx

## start.sh
COPY start.sh .
RUN chmod +x start.sh

EXPOSE 8000
EXPOSE 4242

CMD [ "./start.sh" ]
