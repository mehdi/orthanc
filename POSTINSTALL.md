This app packages the Orthanc Dicom server with the OHIF Dicom viewer.

The OHIF Viewer is accessible at the `/` URI.
Orthanc is accessible at the `/orthanc/` URI, and its DicomWeb and WADO interfaces at `/orthanc/dicom-web` & `/orthanc/wado` respectively.

By default, when you access things with a web browser, you will be redirected to a page for authentication.
However, you can also use HTTP Basic Auth for use with DicomWeb-compatible apps.

If you are planning on using Orthanc's DICOM interface, you can open a terminal and edit `/app/data/orthanc/config.json`,
to change the AETitle and the Dicom Modalities (in the `DicomModalities` key, with the format: ``)

You can change the AETitle in the `DicomAet` key of the JSON file.

You can change the Dicom Modalities in the `DicomModalities` key of the JSON file, with the format:
```json
{
  "DicomModalities": {
    "Name" : {
      "AET" : "MODALITY-AET",
      "Port" : 104,
      "Host" : "hostname or IP address"
    }
  }
}
```

This file also contains other config options that you may want to change, for performance fine-tuning or customization.

You can also add there other options supported by Orthanc, except those described in `/app/code/orthanc/hardcoded.json`.
These have values necessary to work in this package.

If you want to change the port where the DICOM interface is listening, just change the port forwarding configuration in the Cloudron interface.
