#!/usr/bin/env node
'use strict'

const fs = require('fs')

// language=nginx
const nginxConf = `user cloudron;
worker_processes 4;
worker_priority -10;
pid /run/nginx.pid;
daemon off;

# Send logs to stderr
error_log /dev/stderr warn;

events {
    worker_connections 768;
}

http {
    error_log /dev/stderr warn;
    log_format simple '$remote_addr [$time_local] "$request" $status $body_bytes_sent "$http_referer"';
    access_log /dev/stdout simple;

    types_hash_max_size 2048;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    client_body_temp_path /tmp/client_body 1 2;
    client_body_buffer_size 256k;
    client_body_in_file_only off;
    client_max_body_size 200M;

    proxy_buffering off;
    proxy_cache off;

    proxy_temp_path /tmp/proxy_temp 1 2;
    fastcgi_temp_path /tmp/fastcgi_temp 1 2;
    uwsgi_temp_path /tmp/uwsgi_temp 1 2;
    scgi_temp_path /tmp/scgi_temp 1 2;

    server {
        error_log /dev/stderr warn;
        listen 8000 default_server;

        server_name _;

        location @error401 {
            return 302 " /helper/login.html#redirect=$request_uri";
        }

        location = /check-sso-login {
            internal;
            proxy_pass http://localhost:3000/api/check-login;
            proxy_pass_request_body off;
            proxy_set_header Content-Length "";
        }

#        location = /check-sso-admin {
#            internal;
#            proxy_pass http://localhost:3000/api/check-admin;
#            proxy_pass_request_body off;
#            proxy_set_header Content-Length "";
#        }

        location /orthanc/dicom-web/ {
            auth_request /check-sso-login;
            proxy_pass http://localhost:8042/dicom-web/;
        }

        location /orthanc/ {
            auth_request /check-sso-login;
            proxy_pass http://localhost:8042/;
            error_page 401 = @error401;
        }

        location /helper/ {
            alias  /app/code/client/;
            index index.html;
            try_files $uri $uri/ /index.html;
            add_header Cache-Control "no-store, no-cache, must-revalidate";
            include    '/app/code/nginx/mime.types';
            default_type application/octet-stream;
        }

        location /api/ {
            proxy_pass http://localhost:3000;
        }

        location / {
            alias  /app/code/OHIF/;
            index index.html;
            try_files $uri $uri/ /index.html;
            add_header Cache-Control "no-store, no-cache, must-revalidate";
            include    '/app/code/nginx/mime.types';
            default_type application/octet-stream;
        }
    }
}
`

fs.writeFileSync('/run/blank', '')
fs.writeFileSync('/run/nginx.conf', nginxConf)
