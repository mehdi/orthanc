'use strict'

const fs = require('fs').promises

let users = null

const localAuth = async (username, password) => {
  if (!users) {
    try {
      users = JSON.parse(fs.readFileSync('/app/data/.users.json', 'utf8'))
    } catch (e) {
      console.warn('No .user.json file found. Using default admin:admin.')
      users = [{ username: 'admin', password: 'admin', isAdmin: true }]
    }
  }

  const user = users.find(u => username === u.username)
  if (!user) throw new Error('Unknown user')
  if (user.password !== password) throw new Error('Wrong password') // TODO: scrypt

  return {
    id: user.username,
    username: user.username,
    displayName: user.username,
    email: `${user.username}@local.local`,
    admin: user.isAdmin
  }
}

module.exports = { localAuth }
