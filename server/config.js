'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const { getConfig, changeConfig } = require('../orthanc/config.js')
const { isIp } = require('net')

const AetRegex = /^[A-Za-z0-9_\-.]{1,32}$/
const descriptionRegex = /^[A-Za-z0-9_\-.][A-Za-z0-9_\-. ]{0,63}$/
const hostRegex = /^([a-zA-Z0-9]([a-zA-Z0-9-]*[a-zA-Z0-9])?\.)*[A-Za-z0-9]([A-Za-z0-9-]*[A-Za-z0-9])?$/

module.exports = () => {
  const router = new express.Router()

  const jsonParser = bodyParser.json()

  router.get('/get_config', async (req, res, next) => {
    const conf = await getConfig()
    res.status(200).json({
      DicomAet: conf.DicomAet,
      port: conf.port,
      DicomModalities: Object.keys(conf.DicomModalities).map(k => ({
        description: k,
        AET: conf.DicomModalities[k].AET,
        Port: conf.DicomModalities[k].Port,
        Host: conf.DicomModalities[k].Host
      }))
    })
  })

  router.post('/change_config', jsonParser, async (req, res, next) => {
    const { DicomAet, DicomModalities } = req.body

    // Validation
    if (!AetRegex.test(DicomAet)) return res.status(400).send('Invalid DicomAet')
    if (!Array.isArray(DicomModalities)) return res.status(400).send('Invalid DicomModalities')
    for (const modality of DicomModalities) {
      if (!descriptionRegex.test(modality.description)) return res.status(400).send(`Invalid modality.description : ${modality.description}`)
      if (!AetRegex.test(modality.AET)) return res.status(400).send(`Invalid modality.AET : ${modality.AET}`)
      if (typeof modality.Port !== 'number' || !Number.isInteger(modality.Port) || modality.Port < 1 || modality.Port > 65536) return res.status(400).send(`Invalid modality.Port for ${modality.AET} : ${modality.Port}`)
      if (!hostRegex.test(modality.Host) && !isIp(modality.Host)) return res.status(400).send(`Invalid modality.Host for ${modality.AET} : ${modality.Host}`)
      if (DicomModalities.find(m => m !== modality && m.description === modality.description)) return res.status(400).send(`Non-unique modality.description : ${modality.description}`)
      if (DicomModalities.find(m => m !== modality && m.AET === modality.AET)) return res.status(400).send(`Non-unique modality.AET : ${modality.AET}`)
    }

    // Write new config
    await changeConfig({
      DicomAet,
      DicomModalities: DicomModalities.reduce(
        (acc, m) => ({
          [m.description]: {
            AET: m.AET,
            Port: m.Port,
            Host: m.Host
          },
          ...acc
        }),
        {}
      )
    })
    res.status(200).json({ status: 'ok' })
  })

  return router
}
