#!/bin/bash

set -eu

export NODE_ENV=production

mkdir -p /app/data/orthanc/db/

# Copy the default Orthanc settings to a user-modifiable place
if [ ! -f /app/data/orthanc/config.json ]; then
  cp /app/code/orthanc/default.json /app/data/orthanc/config.json
fi

# Creating a secret for passport's sessions
if [ ! -f /app/data/session.secret ]; then
  dd if=/dev/urandom bs=256 count=1 | base64 >/app/data/session.secret
fi

mkdir -p /tmp/orthanc/

# Writing config files
node ./nginx/writeNginxConfig.js
node ./orthanc/writeOrthancConfig.js

chown -R cloudron:cloudron /app/data /tmp /run

# Update orthanc DB
/app/code/orthanc/bin/Orthanc --upgrade /run/orthanc.json

echo "Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i River
