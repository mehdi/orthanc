'use strict'

const getCookie = (cname) => {
  const name = cname + '='
  const cookies = document.cookie.split(';').map(s => s.trim())
  for (const cookie of cookies) {
    if (cookie.startsWith(name)) {
      return cookie.substring(name.length, cookie.length)
    }
  }
  return ''
}

window.config = {
  // default: '/'
  routerBasename: '/',
  whiteLabeling: {
    createLogoComponentFn: React => {
      const links = [
        React.createElement('span', { style: { margin: '7px' } }, ['|']),
        React.createElement('a', { href: '/orthanc/' }, ['Orthanc']),
        React.createElement('span', { style: { margin: '7px' } }, ['|']),
        React.createElement('a', { href: '/api/logout' }, ['Logout'])
      ]
      if (getCookie('orthanc-is-admin') === 'true') {
        links.push(React.createElement('span', { style: { margin: '7px' } }, ['|']))
        links.push(React.createElement('a', { href: '/helper/config.html' }, ['Config']))
      }
      return React.createElement(
        'div',
        { className: 'header-brand' },
        [
          React.createElement('a', {
            className: 'header-logo-image',
            href: '/',
            style: {
              background: 'url(/ohif.svg)',
              backgroundSize: 'contain',
              backgroundRepeat: 'no-repeat',
              filter: 'invert(100%)'
            }
          }),
          React.createElement('a', {
            target: 'blank',
            rel: 'noopener noreferrer',
            className: 'header-logo-text',
            href: 'https://ohif.org',
            style: {
              background: 'url(/ohif-text.svg)',
              backgroundSize: 'contain',
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'center center',
              height: '100%'
            }
          }),
          ...links
        ]
      )
    }
  },
  httpErrorHandler: error => {
    if (error.status === 401) {
      window.location = `/helper/login.html#redirect=${window.location.pathname}`
    } else {
      throw error
    }
  },
  extensions: [],
  showStudyList: true,
  filterQueryParam: false,
  servers: {
    dicomWeb: [
      {
        name: 'Orthanc',
        wadoUriRoot: '/orthanc/wado',
        qidoRoot: '/orthanc/dicom-web',
        wadoRoot: '/orthanc/dicom-web',
        qidoSupportsIncludeField: true,
        imageRendering: 'wadors',
        thumbnailRendering: 'wadors',
        enableStudyLazyLoad: true
      }
    ]
  },
  // Extensions should be able to suggest default values for these?
  // Or we can require that these be explicitly set
  hotkeys: [
    // ~ Global
    {
      commandName: 'incrementActiveViewport',
      label: 'Next Viewport',
      keys: ['right']
    },
    {
      commandName: 'decrementActiveViewport',
      label: 'Previous Viewport',
      keys: ['left']
    },
    // Supported Keys: https://craig.is/killing/mice
    // ~ Cornerstone Extension
    { commandName: 'rotateViewportCW', label: 'Rotate Right', keys: ['r'] },
    { commandName: 'rotateViewportCCW', label: 'Rotate Left', keys: ['l'] },
    { commandName: 'invertViewport', label: 'Invert', keys: ['i'] },
    {
      commandName: 'flipViewportVertical',
      label: 'Flip Horizontally',
      keys: ['h']
    },
    {
      commandName: 'flipViewportHorizontal',
      label: 'Flip Vertically',
      keys: ['v']
    },
    { commandName: 'scaleUpViewport', label: 'Zoom In', keys: ['+'] },
    { commandName: 'scaleDownViewport', label: 'Zoom Out', keys: ['-'] },
    { commandName: 'fitViewportToWindow', label: 'Zoom to Fit', keys: ['='] },
    { commandName: 'resetViewport', label: 'Reset', keys: ['space'] },
    // clearAnnotations
    { commandName: 'nextImage', label: 'Next Image', keys: ['down'] },
    { commandName: 'previousImage', label: 'Previous Image', keys: ['up'] },
    // firstImage
    // lastImage
    {
      commandName: 'previousViewportDisplaySet',
      label: 'Previous Series',
      keys: ['pagedown']
    },
    {
      commandName: 'nextViewportDisplaySet',
      label: 'Next Series',
      keys: ['pageup']
    },
    // ~ Cornerstone Tools
    { commandName: 'setZoomTool', label: 'Zoom', keys: ['z'] },
    // ~ Window level presets
    {
      commandName: 'windowLevelPreset1',
      label: 'W/L Preset 1',
      keys: ['1']
    },
    {
      commandName: 'windowLevelPreset2',
      label: 'W/L Preset 2',
      keys: ['2']
    },
    {
      commandName: 'windowLevelPreset3',
      label: 'W/L Preset 3',
      keys: ['3']
    },
    {
      commandName: 'windowLevelPreset4',
      label: 'W/L Preset 4',
      keys: ['4']
    },
    {
      commandName: 'windowLevelPreset5',
      label: 'W/L Preset 5',
      keys: ['5']
    },
    {
      commandName: 'windowLevelPreset6',
      label: 'W/L Preset 6',
      keys: ['6']
    },
    {
      commandName: 'windowLevelPreset7',
      label: 'W/L Preset 7',
      keys: ['7']
    },
    {
      commandName: 'windowLevelPreset8',
      label: 'W/L Preset 8',
      keys: ['8']
    },
    {
      commandName: 'windowLevelPreset9',
      label: 'W/L Preset 9',
      keys: ['9']
    }
  ],
  cornerstoneExtensionConfig: {}
}
