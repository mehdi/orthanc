/* global Vue */

import Config from './config.vue'

window.app = new Vue({
  el: '#app',
  render: h => h(Config),
  localStorage: {
    filesShowUseless: {
      type: Boolean,
      default: false
    },
    seriesShowLast: {
      type: Boolean,
      default: true
    }
  }
})
