/* global Vue */

import Login from './login.vue'

window.app = new Vue({
  el: '#app',
  render: h => h(Login),
  localStorage: {
    filesShowUseless: {
      type: Boolean,
      default: false
    },
    seriesShowLast: {
      type: Boolean,
      default: true
    }
  }
})
