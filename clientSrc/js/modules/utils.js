export const parseQueryString = queryString => queryString.split('&')
  .map(str => str.split('='))
  .reduce((obj, pair) => Object.assign(obj, { [pair[0]]: pair.length === 1 ? true : pair[1] }), {})
