const fetchOptions = async ({ body, headers = {}, method = 'POST' } = {}) => {
  return {
    method,
    credentials: 'same-origin',
    headers: {
      // 'X-CSRFToken': csrfToken
      ...headers
    },
    body
  }
}

const jsonFetchOptions = obj => fetchOptions({
  body: JSON.stringify(obj),
  headers: { 'Content-Type': 'application/json' }
})

export const logout = () => {
  window.location.href = '/api/logout'
}

export const login = async (username, password) => {
  const response = await fetch(
    '/api/login',
    await jsonFetchOptions({ username, password })
  )
  if (response.status === 401) throw new Error('Unauthorized')
  return response.json()
}

export const checkLogin = async () => {
  const response = await fetch(
    '/api/check-login',
    await fetchOptions({ method: 'GET' })
  )
  if (response.status === 401) return null
  else if (response.status === 200) return response.json()
  else throw new Error('Unexpected error')
}

export const checkAdmin = async () => {
  const response = await fetch(
    '/api/check-admin',
    await fetchOptions({ method: 'GET' })
  )
  if (response.status === 401) return null
  else if (response.status === 403) throw new Error('Unauthorized. This is only for Admins.')
  else if (response.status === 200) return response.json()
  else throw new Error('Unexpected error')
}

export const getConfig = async () => {
  const response = await fetch(
    '/api/config/get_config',
    await fetchOptions({ method: 'GET' })
  )
  if (response.status === 401) throw new Error('Unauthorized')
  if (response.status !== 200) throw new Error('Error getting config: ' + response.status)
  return response.json()
}

export const changeConfig = async (config) => {
  const response = await fetch(
    '/api/config/change_config',
    await jsonFetchOptions(config)
  )
  if (response.status === 401) throw new Error('Unauthorized')
  if (response.status !== 200) throw new Error('Error changing config: ' + response.status)
  return response.json()
}
