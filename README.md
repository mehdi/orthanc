# Orthanc

## TODO
- https://book.orthanc-server.com/plugins/postgresql.html

## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=com.orthanc-server.orthanc)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.orthanc-server.orthanc
```

## Building

### Cloudron
The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
git clone https://git.cloudron.io/mehdi/orthanc.git
cd orthanc
cloudron build
cloudron install
```
